;;;callwcc.scm callcc ex from https://ds26gte.github.io/tyscheme/index-Z-H-15.html

(iota 9)
(define callwcc call-with-current-continuation)
(define l (iota 99999))

(define list-product-no-cwcc
  (lambda (s)
    (let recur ((s s))
      (if (null? s) 1
          (* (car s) (recur (cdr s)))))))

(define list-product-with-cwcc
  (lambda (s)
    (callwcc
      (lambda (escape)
        (let recur ((s s))
          (if (null? s) 1
              (if (= (car s) 0) (escape 0)
                  (* (car s) (recur (cdr s))))))))))
