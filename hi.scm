;;;hi.scm hindi devanagari fn wrappers n syntx for guile scheme
;;;copyright (c) शक॰ १९४५ raj @ al . under gplv3 . without warranty

(define-syntax निरूपय (syntax-rules () ((निरूपय e b)(define e b))))
(define-syntax स्थापय (syntax-rules () ((स्थापय e b)(set! e b))))
(define-syntax आरम्भय (syntax-rules () ((आरम्भय e e* ...) (begin e e* ...))))
(define-syntax करण (syntax-rules () ((करण e b)(lambda e b))))
(define-syntax यथावस्था (syntax-rules () ((यथावस्था e e* ...)(cond e e* ...))))
(define-syntax यदि (syntax-rules ()((यदि c t e)(if c t e))))
(define-syntax क्रमलेख (syntax-rules () ((क्रमलेख e)(quote e))))
;;;कुछ प्रकरणों में क्रमलेख के स्थान पर चिह्न का प्रयोग उचित।
(define-syntax चिह्न (syntax-rules () ((चिह्न e)(quote e))))
(define-syntax अथच (syntax-rules () ((अथच e e* ...)(and e e* ...))))
(define-syntax अथवा (syntax-rules () ((अथवा e e* ...)(or e e* ...))))
(define-syntax सह (syntax-rules () ((सह v b)(let* v b))))

;;;गणित।
(निरूपय सं +)
(निरूपय घ्न *)
(निरूपय व्यव -)
(निरूपय हृ /)
(निरूपय एकाधिक 1+)
(निरूपय एकोन 1-)
(निरूपय (भाग x y) (inexact->exact (truncate-quotient x y)))
(निरूपय शेष truncate-remainder)
(निरूपय वर्गमूल sqrt)
(निरूपय घात expt)
(निरूपय घातञ exp)
(निरूपय छेदञ log)
(निरूपय छेददश log10)
(निरूपय अमुकांक random)
(निरूपय शेषांकत्याजय truncate)
;;;(निरूपय पूर्णांक inexact->exact)

;;;सूचीगत।
(निरूपय सूची list)
(निरूपय सूचीरचय make-list)
(निरूपय अग्र car)
(निरूपय इतर cdr)
(निरूपय रचय cons)
(निरूपय सूचीमान length)
(निरूपय आचर apply)
(निरूपय (शून्यादि n) (iota (+ n 1)))
(निरूपय (एकादि ख) (इतर (शून्यादि ख)))
(निरूपय प्रतिफल map)
(निरूपय प्रतिकरण for-each)

;;;एकदावर्तनविशेष।
(use-modules (ice-9 threads))
(निरूपय क्रमवर्तकगणन current-processor-count)
(निरूपय एकदाप्रतिफल par-map)
(निरूपय एकदाप्रतिकरण par-for-each)

;;;तर्कगत।
(निरूपय सत्य #t)
(निरूपय असत्य #f)
(निरूपय समंवा eq?)
(निरूपय तुल्यवा equal?)
(निरूपय नास्तिवा null?)
(निरूपय रिक्तंवा null?)
(निरूपय शून्यंवा zero?)
(निरूपय अंकःवा number?)
(निरूपय चिह्नंवा symbol?)
(निरूपय क्रमलेखःवा symbol?)
(निरूपय नेति not)
(निरूपय न्यूनंवा <)
(निरूपय समंन्यूनंवा <=)
;;; यथावस्था क्रमरूप में प्रयुक्त
(निरूपय अन्यथा सत्य)

;;;वार्तालाप।
(निरूपय कथय display)
(निरूपय नव्यरेखा newline)
(निरूपय आगतद्वार (current-input-port))
(निरूपय निर्गतद्वार (current-output-port))
(निरूपय अपक्रमद्वार (current-error-port))

(use-modules (ice-9 format))
(निरूपय प्रारूप format)
(निरूपय (प्रतिशतप्रारूप ख)
       (प्रारूप असत्य "~2$" ख))
(निरूपय (प्रतिलक्षप्रारूप ख)
       (प्रारूप असत्य "~5$" ख))
(निरूपय (प्रतिकोटिप्रारूप ख)
       (प्रारूप असत्य "~7$" ख))

;;;सूत्रगत।
(निरूपय सूत्रंवा string?)
(निरूपय सूत्र string)
(निरूपय सूच्यात्सूत्र list->string)
(निरूपय सूत्रात्सूची string->list)
(निरूपय अंकात्सूत्र number->string)
(निरूपय सूत्रयोजय string-join)
(निरूपय सूत्रस्फुटय string-split)
(निरूपय सूत्रमान string-length)
(निरूपय सूत्रस्थ string-ref)
(निरूपय सूत्रानुकृति string-copy)
(निरूपय सूत्राम्श substring)
(निरूपय सूत्राधियोजय string-pad)
(निरूपय सूत्रदक्षिणाधियोजय string-pad-right)
(निरूपय सूत्रपरिच्छेदय string-trim-both)
(निरूपय सूत्रस्थापय string-set!)
(निरूपय सूत्रपूरय string-fill!)
(निरूपय सूत्राम्शपूरय substring-fill!)
(निरूपय सूत्राम्शविस्थापय substring-move!)
(निरूपय सूत्रन्यूनंवा string<?)
(निरूपय सूत्रसमंन्यूनंवा string<=?)
(निरूपय सूत्रविभेदक string-hash)
(निरूपय सूत्रसूचकांक string-index)
(निरूपय सूत्रोपसर्गःवा string-prefix?)
(निरूपय सूत्रप्रत्ययःवा string-suffix?)
(निरूपय सूत्रगणन string-count)
(निरूपय सूत्राम्शस्थ string-contains)
(निरूपय सूत्रव्यतिक्रम string-reverse)
(निरूपय सूत्रोत्तरयोजय string-append)
(निरूपय सूत्रप्रतिकरण string-map)
(निरूपय सूत्रप्रतिस्थापय string-replace)
(निरूपय सूत्रविघटय string-tokenize)
(निरूपय सूत्रक्षालय string-filter)
(निरूपय सूत्रलोपय string-delete)

;;;कोशगत।
(निरूपय कोशरचय make-hash-table)
(निरूपय कोशःवा hash-table?)
(निरूपय कोशमार्जय hash-clear!)
(निरूपय कोशस्थ hash-ref)
(निरूपय कोशस्थापय hash-set!)
(निरूपय कोशलोपय hash-remove!)
(निरूपय कोशवस्तुप्रापय hash-get-handle)
(निरूपय कोशवस्तुरचय hash-create-handle!)
(निरूपय कोशात्सूची hash-map->list)
(निरूपय कोशगणन hash-count)

;;;सामान्य सुविधाएँ।
(निरूपय मिश्रय merge)
(निरूपय क्रमशःवा sorted?)
(निरूपय क्रमशः sort)
(निरूपय वस्त्वात्सूत्र object->string)
(निरूपय यन्त्रसमय current-time)
(निरूपय फल eval)
(निरूपय क्रमप्रसंग interaction-environment)
(निरूपय क्रमसमय times)
(निरूपय वास्तविकसमय tms:clock)
(निरूपय उपयोगकर्तासमय tms:utime)
(निरूपय संचालकसमय tms:stime)
(निरूपय अनुवर्तनसह call-with-current-continuation)
(निरूपय भारय load)

;;;अभिलेखगत।
(use-modules (ice-9 textual-ports))
(निरूपय सूत्रसर्वग्राहय get-string-all)
(निरूपय सूत्ररेखाग्राहय get-line)
(निरूपय सूत्रप्रस्थापय put-string)
(निरूपय आगताभिलेखसह call-with-input-file)
(निरूपय निर्गताभिलेखसह call-with-output-file)

(use-modules (ice-9 popen))
(define (call-with-pipe pstr fn)
  (begin
    (define p (open-input-output-pipe pstr))
    (define res (fn p))
    (close-pipe p)
    res))

;;;depends on external utility gnurl
(define (gnurl-http-get url)
  (call-with-pipe (string-join (list "gnurl" url)) get-string-all))

;;;बाह्यक्रमकों के लिए।
(निरूपय प्रणालीसह call-with-pipe)

;;;जालगत
(निरूपय जालग्राहय gnurl-http-get)


;;;संख्या लिप्यान्तरण।
;;;वस्तुतः रू  रूं  यी हृ होना चाहिए।
(define n "+-i/e0123456789.")
(define s "सऋईहघ०१२३४५६७८९:")

(define (pairup x y) (map (lambda (i j) (cons i j)) x y))
(define ns (pairup (string->list n) (string->list s)))
(define sn (pairup (string->list s) (string->list n)))

(define (सूत्रात्संख्या n) (list->string(map (lambda(e) (cdr (assoc e ns))) (string->list n))))
(define (संख्या n) (सूत्रात्संख्या (अंकात्सूत्र n)))

(define (अंक s) (string->number (list->string(map (lambda(e) (cdr (assoc e sn))) (string->list s)))))

(निरूपय दश   (अंक "१०"))
(निरूपय शत   (अंक "१००"))
(निरूपय सहस्र (अंक "१०००"))
(निरूपय लक्ष    (घ्न शत सहस्र))
(निरूपय कोटि   (घ्न शत लक्ष))
(निरूपय प्रयुत   (घ्न दश लक्ष))
(निरूपय अब्ज    (घ्न सहस्र प्रयुत))
(निरूपय महापद्म (घ्न सहस्र अब्ज))

(निरूपय (हृसहस्र ख)  (हृ ख सहस्र))
(निरूपय (हृलक्ष ख)   (हृ ख लक्ष))
(निरूपय (हृकोटि ख)  (हृ ख कोटि))
(निरूपय (घ्नसहस्र ख) (घ्न ख सहस्र))
(निरूपय (घ्नलक्ष ख)  (घ्न ख लक्ष))
(निरूपय (घ्नकोटि ख) (घ्न ख कोटि))

(निरूपय (अव्यर्थ ख)
       (यथावस्था
	((तुल्यवा (अंक "२") (सूचीमान ख)) ख)
	((तुल्यवा "०" (अग्र ख)) (अव्यर्थ (इतर (इतर ख))))
	(अन्यथा ख)))

(निरूपय (समयान्तरण ख)
       (सह ((गु (हृ (अंक "४८६०००") (अंक "८६४००")))
	    (संनि (घ्न ख गु))
	    (अहो (संख्या (भाग संनि (अंक "४८६०००"))))
	    (अहोशेष (शेष संनि (अंक "४८६०००")))
	    (मु (संख्या (भाग अहोशेष (अंक "१६२००"))))
	    (मुशेष (शेष अहोशेष (अंक "१६२००")))
	    (कल (संख्या (भाग मुशेष (अंक "५४०"))))
	    (कलशेष (शेष मुशेष (अंक "५४०")))
	    (का (संख्या (भाग कलशेष (अंक "१८"))))
	    (काशेष (शेष कलशेष (अंक "१८")))
	    (नि (सूत्रात्संख्या (प्रतिलक्षप्रारूप काशेष))))
    (अव्यर्थ
	 (सूची अहो (चिह्न अहोरात्र)
	      मु (चिह्न मुहूर्त)
	      कल (चिह्न कला)
	      का (चिह्न काष्ठा)
	      नि (चिह्न निमेष)))))

;;;योजन दण्ड अंगुल अनुपात स्पष्ट है परन्तु पाश्चात्य देशमान का अनुपात अनुसंधान से प्राप्त अनुमान है। अतः शोधन सम्भाव्य।
(निरूपय (देशमानान्तरण ख)
       (सह ((संअं (हृ ख (अंक "०:०१७६")))
	    (यो (संख्या (भाग संअं (अंक "७६८०००"))))
	    (योशेष (शेष संअं (अंक "७६८०००")))
	    (द (संख्या (भाग योशेष (अंक "९६"))))
	    (दशेष (शेष योशेष (अंक "९६")))
	    (अं (सूत्रात्संख्या (प्रतिलक्षप्रारूप दशेष))))
	   (अव्यर्थ (सूची यो (चिह्न योजन)
		       द (चिह्न दण्ड)
		       अं (चिह्न अंगुल)))))

(निरूपय (भारान्तरण ख)
       (सह ((संर (हृ ख (अंक "०:१७०३१२५")))
	    (ध (संख्या (भाग संर (अंक "३२००"))))
	    (धशेष (शेष संर (अंक "३२००")))
	    (क (संख्या (भाग धशेष (अंक "८०"))))
	    (कशेष (शेष धशेष (अंक "८०")))
	    (र (सूत्रात्संख्या (प्रतिलक्षप्रारूप कशेष))))
	   (अव्यर्थ (सूची ध (चिह्न धरण)
		       क (चिह्न कर्ष)
		       र (चिह्न रत्ती)))))

(निरूपय (रजतभार ख)
       (सह ((संर (हृ ख (अंक "०:१७०३१२५")))
	    (श (संख्या (भाग संर (अंक "३२०"))))
	    (शशेष (शेष संर (अंक "३२०")))
	    (र (सूत्रात्संख्या (प्रतिलक्षप्रारूप शशेष))))
	   (अव्यर्थ (सूची श (चिह्न शतमान)
		       र (चिह्न रत्ती)))))

(निरूपय (तोलन ख)
       (सह ((संर (हृ ख (अंक "०:१७०३१२५")))
	    (तो (संख्या (भाग संर (अंक "९६"))))
	    (तोशेष (शेष संर (अंक "९६")))
	    (र (सूत्रात्संख्या (प्रतिलक्षप्रारूप तोशेष))))
	   (अव्यर्थ (सूची तो (चिह्न तोला)
		       र (चिह्न रत्ती)))))

(निरूपय (समयमान ख)
  (आरम्भय
    (निरूपय प्र (क्रमसमय))
    (फल ख (क्रमप्रसंग))
    (निरूपय द्व (क्रमसमय))
    ;;(कथय द्व)
    ;;(कथय प्र)
    (कथय "वास्तविक। ")
    (कथय (समयान्तरण (हृसहस्र (व्यव (वास्तविकसमय द्व) (वास्तविकसमय प्र)))))
    ;;(कथय "। उपयोगकर्ता। ")
    ;;(कथय (समयान्तरण (हृसहस्र (व्यव (उपयोगकर्तासमय द्व) (उपयोगकर्तासमय प्र)))))
    ;;(कथय "। संचालक। ")
    ;;(कथय (समयान्तरण (हृसहस्र (व्यव (संचालकसमय द्व) (संचालकसमय प्र)))))
    (कथय "।")
    (नव्यरेखा)))
